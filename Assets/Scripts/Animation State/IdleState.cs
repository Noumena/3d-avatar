﻿using UnityEngine;
using SingleTon.scripts;

public class IdleState : StateBase
{
    public IdleState(PlayerController controller, Animator animator) : base(controller, animator)
    {
    }

    public override PlayerState GetState()
    {
        return PlayerState.Idle;
    }

    public override void OnTransitionIn()
    {
        animator.SetTrigger("IsIdle");
        controller.ResetPlayerMove();
    }

    public override void UpdateState()
    {
        controller.UpdateMovement();
    }
}

