﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class SitIdleState : StateBase
{
    public SitIdleState(PlayerController controller, Animator animator) : base(controller, animator)
    {
    }

    public override PlayerState GetState()
    {
        return PlayerState.SitIdle;
    }

    public override void OnTransitionIn()
    {
        
    }

    public override void UpdateState()
    {
        
    }
}
