﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class SitState : StateBase
{
    public SitState(PlayerController controller, Animator animator) : base(controller, animator)
    {
    }

    public override PlayerState GetState()
    {
        return PlayerState.Sit;
    }

    public override void OnTransitionIn()
    {
        animator.SetTrigger("IsIdle");
        controller.ResetPlayerMove();
        animator.SetTrigger("IsSitting");
    }

    public override void UpdateState()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f)
        {
            controller.ChangeState(PlayerState.SitIdle);
        }
    }
}
