﻿using UnityEngine;
using SingleTon.scripts;

public class StandState : StateBase
{
    public StandState(PlayerController controller, Animator animator) : base(controller, animator)
    {

    }

    public override PlayerState GetState()
    {
        return PlayerState.Stand;
    }

    public override void OnTransitionIn()
    {
        animator.SetTrigger("IsStanding");
    }

    public override void UpdateState()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            controller.ChangeState(PlayerState.Idle);
        }
    }
}
