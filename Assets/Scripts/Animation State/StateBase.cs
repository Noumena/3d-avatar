﻿using System;
using UnityEngine;
using SingleTon.scripts;

public abstract class StateBase
{
    public StateBase(PlayerController controller, Animator animator)
    {
        this.controller = controller;
        this.animator = animator;
    }

    public abstract PlayerState GetState();
    public abstract void UpdateState();
    public abstract void OnTransitionIn();

    protected Animator animator;
    protected PlayerController controller;

    internal void OnTransitionOut()
    {

    }
}
