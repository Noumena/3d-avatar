﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class TalkingState : StateBase
{
    public TalkingState(PlayerController controller, Animator animator) : base(controller, animator)
    {

    }

    public override PlayerState GetState()
    {
        return PlayerState.Talking;
    }

    public override void OnTransitionIn()
    {
        animator.SetTrigger("IsTalking");
    }

    public override void UpdateState()
    {

    }
}
