﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class WalkState : StateBase
{
    public WalkState(PlayerController controller, Animator animator) : base(controller, animator)
    {
    }

    public override PlayerState GetState()
    {
        return PlayerState.Walk;
    }

    public override void OnTransitionIn()
    {
        animator.SetTrigger("IsWalking");
    }

    public override void UpdateState()
    {
        controller.UpdateMovement();
    }
}
