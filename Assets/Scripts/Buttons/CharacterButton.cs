﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SingleTon.scripts;

public class CharacterButton : MonoBehaviour
{
    [SerializeField] private CharacterType characterType;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClickSelectedCharacter);
    }

    public void OnClickSelectedCharacter()
    {
        CharacterManager.Instance.SetCurrentCharacter(characterType); 
    }
}
