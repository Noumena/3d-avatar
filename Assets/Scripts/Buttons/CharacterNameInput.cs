﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SingleTon.scripts;

public class CharacterNameInput : MonoBehaviour
{ 
    private void Start()
    {
        GetComponent<InputField>().onValueChanged.AddListener(SetInputFieldName);
    }
    
    private void SetInputFieldName(string name)
    {
        CharacterManager.Instance.SetCharacterName(name);
    }
}
