﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SingleTon.scripts;

public class QuitButton : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(quit);
    }

    private void quit()
    {
        UIManager.Instance.OpenPanel(UIPanel.QuitPanel);
        Time.timeScale = 0;
    }
}
