﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SingleTon.scripts;

public class SelectedButton : MonoBehaviour
{
    [SerializeField] private InputField characterInputField;
    
    private void Start()
    {        
        GetComponent<Button>().onClick.AddListener(OnClickSelectedButton);      
    }

    public void OnClickSelectedButton()
    {
        CharacterManager characterManager = CharacterManager.Instance;

        if (characterManager)
        {
            characterManager.CreateCurrentCharacter(characterManager.GetCurrentCharacter().CharacterType);
        }      
    }
}
