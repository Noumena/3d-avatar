﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float followHeight = 5f;
    [SerializeField] private float followDistance = 3f;

    private Transform player;

    private float targetHeight;
    private float currentHeight;
    private float currentRotation;

    private void Start()
    {
        player = PlayerController.Instance.transform;
    }

    private void LateUpdate()
    { 
        if (player)
        {
            targetHeight = player.position.y + followHeight;
            currentRotation = transform.eulerAngles.y;
            currentHeight = Mathf.Lerp(transform.position.y, targetHeight, 0.9f * Time.deltaTime);

            Quaternion euler = Quaternion.Euler(new Vector3(0f, currentRotation, 0f));
            Vector3 targetPos = player.position - (euler * Vector3.forward) * followDistance;

            targetPos.y = currentHeight;
            transform.position = targetPos;
            transform.LookAt(player);
        }       
    }
}
