﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue 
{
    [SerializeField] string name;

    public string Name
    {
        get { return name; }
    }

    [TextArea(3, 10)]
    [SerializeField]private string[] sentences;

    public string[] Sentences
    {
        get { return sentences; }
    }
}
