﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;
    private PlayerController player;
   
    private void OnMouseDown()
    {
        player = PlayerController.Instance;

        if (player)
        {
            if (Vector3.Distance(transform.position, player.transform.position) <= 4f)
            {
                UIManager.Instance.OpenPanel(UIPanel.DialoguePanel);
                DialogueManager.Instance.StartDialogue(dialogue);
                player.ChangeState(PlayerState.Talking);
            }           
        }      
    }
}
