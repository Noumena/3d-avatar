﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum CharacterType
{
    AJ = 0,
    Kaya = 1
}

namespace SingleTon.scripts
{
    public class CharacterManager : SingletonBase<CharacterManager>
    {
        [SerializeField] private CharacterInfo ajPrefab, kayaPrefab;

        [SerializeField] private Transform SpawnPoint;

        private string characterName;

        private Dictionary<CharacterType, CharacterInfo> characters = new Dictionary<CharacterType, CharacterInfo>();
        
        private CharacterInfo currentCharacter = null;

        private CharacterInfo selectedCharacter = new CharacterInfo();

        protected override void Awake()
        {
            persist = true;
            base.Awake();
        }

        public void Start()
        {
            if (SpawnPoint && ajPrefab && kayaPrefab)
            {
                CharacterInfo aj = Instantiate(ajPrefab, SpawnPoint.transform.position, Quaternion.identity);
                CharacterInfo kaya = Instantiate(kayaPrefab, SpawnPoint.transform.position, Quaternion.identity);

                characters.Add(CharacterType.AJ, aj);
                characters.Add(CharacterType.Kaya, kaya);

                characters[CharacterType.AJ].gameObject.SetActive(false);
                characters[CharacterType.Kaya].gameObject.SetActive(false);

                SetCurrentCharacter(CharacterType.AJ);
            }
        }

        public void SetCurrentCharacter(CharacterType name)
        {
            if(currentCharacter != null)
            {
               currentCharacter.gameObject.SetActive(false);
            }

            currentCharacter = characters[name];
            currentCharacter.gameObject.SetActive(true);
        }

        public void CreateCurrentCharacter(CharacterType characterType)
        {
            selectedCharacter.Name = characterName;
            selectedCharacter.CharacterType = characterType;
            SceneManager.LoadScene(1);                    
        }

        public CharacterInfo GetCurrentCharacter()
        {
            return currentCharacter;
        }

        public CharacterInfo GetSelectedCharacter()
        {
            return selectedCharacter;
        }

        public void SetCharacterName(string name)
        {
            characterName = name;            
        }
    }
}
