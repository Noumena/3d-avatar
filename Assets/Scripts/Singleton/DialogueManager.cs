﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SingleTon.scripts
{
    public class DialogueManager : SingletonBase<DialogueManager>
    {
        private Queue<string> sentences;
        [SerializeField] private Text dialogueText;
        private Dialogue dialogue;

        protected override void Awake()
        {
            base.Awake();
        }

        void Start()
        {
            sentences = new Queue<string>();
        }

        public void StartDialogue(Dialogue dialogue)
        {            
            sentences.Clear();

            foreach (string sentence in dialogue.Sentences)
            {
               sentences.Enqueue(sentence);
            }

            this.dialogue = dialogue;
            DisplayerNextSentence();           
        }

        public void DisplayerNextSentence()
        {
            if (dialogueText)
            {
                if (sentences.Count == 0)
                {
                    EndDiaLogue();
                }
                else
                {
                    string sentence = sentences.Dequeue();
                    dialogueText.text = dialogue.Name +" : " + sentence;
                }
            }
        }

        private void EndDiaLogue()
        {
            UIManager.Instance.HidePanel(UIPanel.DialoguePanel);
            PlayerController.Instance.ChangeState(PlayerState.Idle);
        }
    }
}

