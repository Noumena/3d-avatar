﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using SingleTon.scripts;

public enum PlayerState
{
    Idle = 0,
    Stand = 1,
    Walk = 2,
    Sit = 3,
    SitIdle = 4,
    Talking = 5
}
namespace SingleTon.scripts
{
    public class PlayerController : SingletonBase<PlayerController>
    {
        private Animator anim;

        [SerializeField]
        private float moveSpeed = 5f, offSetMagnitude = 0.5f;

        Dictionary<PlayerState, StateBase> states = new Dictionary<PlayerState, StateBase>();
        public StateBase currentState = null;

        private CollisionFlags collisionFlags = CollisionFlags.None;
        private CharacterController charController;

        private float gravity = 9.8f;
        private float height;
        private float PointToDistance;

        private bool CanMove;
        private bool isMouseMove;
        
        private Vector3 TargetPos = Vector3.zero;
        private Vector3 PlayerMove = Vector3.zero;

        protected override void Awake()
        {
            base.Awake();
            anim = GetComponent<Animator>();
            charController = GetComponent<CharacterController>();
        }

        private void Start()
        {
            SetAnimationState();
        }

        private bool IsGround()
        {
            if (collisionFlags == CollisionFlags.Below)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CalculatingHeight()
        {
            if (IsGround())
            {
                height = 0f;
            }
            else
            {
                height -= gravity * Time.deltaTime;
            }
        }

        private void SetAnimationState()
        {
            StateBase idleState = new IdleState(this, anim);
            StateBase walkState = new WalkState(this, anim);
            StateBase sitState = new SitState(this, anim);
            StateBase sitIdleState = new SitIdleState(this, anim);
            StateBase standState = new StandState(this, anim);
            StateBase talkingState = new TalkingState(this, anim);

            states.Add(idleState.GetState(), idleState);
            states.Add(walkState.GetState(), walkState);
            states.Add(sitState.GetState(), sitState);
            states.Add(sitIdleState.GetState(), sitIdleState);
            states.Add(standState.GetState(), standState);
            states.Add(talkingState.GetState(), talkingState);

            currentState = idleState;
        }

        public void ResetPlayerMove()
        {
            PlayerMove.Set(0f, 0f, 0f);
        }

        void Update()
        {
            currentState.UpdateState();
        }

        public void UpdateMovement()
        {
            CalculatingHeight();
            MovePlayer();        
            PlayerMove.y = height * Time.deltaTime;
            collisionFlags = charController.Move(PlayerMove);
        }

        public void Sit()
        {
            UIManager.Instance.HidePanel(UIPanel.SitPanel);
            ChangeState(PlayerState.Sit);
        }

        private void MovePlayer()
        {
            keyboardInput();
            Move();
        }
            
        private void keyboardInput()
        {
            if (Input.GetKey(KeyCode.W))
            {
                CanMove = true;
                TargetPos = transform.position + new Vector3(0f, 0f, -0.5f);
            }

            if (Input.GetKey(KeyCode.A))
            {
                CanMove = true;
                TargetPos = transform.position + new Vector3(0.5f, 0f, 0f);
            }

            if (Input.GetKey(KeyCode.S))
            {
                CanMove = true;
                TargetPos = transform.position + new Vector3(0f, 0f, 0.5f);
            }

            if (Input.GetKey(KeyCode.D))
            {
                CanMove = true;
                TargetPos = transform.position + new Vector3(-0.5f, 0f, 0f);
            }
        }

        private void Move()
        {
            if (CanMove)
            {
                ChangeState(PlayerState.Walk);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(TargetPos - transform.position), 15f * Time.deltaTime);
                PlayerMove = transform.forward * moveSpeed * Time.deltaTime;

                if (Vector3.Distance(transform.position, TargetPos) <= offSetMagnitude)
                {
                    CanMove = false;
                }
            }
            else
            {
                ChangeState(PlayerState.Idle);
            }
        }

        public void ChangeState(PlayerState state)
        {
            currentState.OnTransitionOut();
            currentState = states[state];
            currentState.OnTransitionIn();
        }

        private void OnMouseDown()
        {
            if (currentState.GetState() == PlayerState.SitIdle)
            {
                UIManager.Instance.OpenPanel(UIPanel.StandPanel);
            }
        }

        public void Stand()
        {
            UIManager.Instance.HidePanel(UIPanel.StandPanel);

            if (currentState.GetState() == PlayerState.SitIdle)
            {
                ChangeState(PlayerState.Stand);
            }
        }
    }
}

