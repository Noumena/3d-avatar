﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SingleTon.scripts
{
    public class ScoreManager : SingletonBase<ScoreManager>
    {
        [SerializeField] private Text scoreText;

        private const string scoreLabel = "Score: ";

        private int currentScore = 0;

        protected override void Awake()
        {
            base.Awake();

            UpdateScore();
        }

        public void AddScore(int score)
        {
            currentScore += score;
            UpdateScore();
        }

        private void UpdateScore()
        {
            if (scoreText)
            {
                scoreText.text = scoreLabel + currentScore.ToString();
            }
        }      
    }
}

