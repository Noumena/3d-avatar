﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SingleTon.scripts
{
    public abstract class SingletonBase<T> : MonoBehaviour where T : SingletonBase<T>
    {        
        protected bool persist = false;

        private static T instance;
        public static T Instance { get { return instance; } }

        protected virtual void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = (T)this;

                if (persist)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
        }

        protected virtual void OnDestroy()
        {
            instance = null;
        }

        void OnApplicationQuit()
        {
            instance = null;
        }
    }
}
