﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UIPanel
{
    SitPanel = 0,
    StandPanel = 1,
    DialoguePanel = 2,
    QuitPanel = 3
}
namespace SingleTon.scripts
{
    public class UIManager : SingletonBase<UIManager>
    {
        [SerializeField] private Panel sitPanel, standPanel, dialoguePanel, quitPanel;
        private Dictionary<UIPanel, Panel> uiPanels;

        protected override void Awake()
        {
            base.Awake();
        }

        private void Start()
        {
            uiPanels = new Dictionary<UIPanel, Panel>();

            if (sitPanel && standPanel && dialoguePanel && quitPanel)
            {
                uiPanels.Add(UIPanel.SitPanel, sitPanel);
                uiPanels.Add(UIPanel.StandPanel, standPanel);
                uiPanels.Add(UIPanel.DialoguePanel, dialoguePanel);
                uiPanels.Add(UIPanel.QuitPanel, quitPanel);
            }
        }

        public void OpenPanel(UIPanel panel)
        {
            Panel selectedPanel = uiPanels[panel];

            if (selectedPanel)
            {
                selectedPanel.gameObject.SetActive(true);
            }
        }

        public void HidePanel(UIPanel panel)
        {
            Panel selectedPanel = uiPanels[panel];

            if (selectedPanel)
            {
                selectedPanel.gameObject.SetActive(false);
            }
        }

    }
}

