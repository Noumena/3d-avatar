﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class DialoguePanel : Panel
{
    public override void ClosePanel()
    {
        UIManager.Instance.HidePanel(uiPanel);
    }
}
