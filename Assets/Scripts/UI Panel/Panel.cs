﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public abstract class Panel : MonoBehaviour
{
    [SerializeField] protected UIPanel uiPanel;

    public UIPanel UIpanel
    {
        get { return uiPanel; }
    }

    public abstract void ClosePanel();
}
