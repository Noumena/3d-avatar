﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class QuitPanel : Panel
{

    public override void ClosePanel()
    {
        UIManager.Instance.HidePanel(uiPanel);
        Time.timeScale = 1;
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
