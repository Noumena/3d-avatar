﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class SitPanel : Panel
{ 
    public override void ClosePanel()
    {
        UIManager.Instance.HidePanel(uiPanel);
    }

    public void Sit()
    {
        PlayerController.Instance.Sit();
    }
}
