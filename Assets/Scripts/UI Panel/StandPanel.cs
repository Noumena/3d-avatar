﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class StandPanel : Panel
{
    public override void ClosePanel()
    {
        UIManager.Instance.HidePanel(uiPanel);
    }

    public void Stand()
    {
        PlayerController.Instance.Stand();
    }
}
