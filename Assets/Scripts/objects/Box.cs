﻿using UnityEngine;
using SingleTon.scripts;

public class Box : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();

        if (player)
        {
            UIManager.Instance.OpenPanel(UIPanel.SitPanel);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerController player = other.gameObject.GetComponent<PlayerController>();

        if (player)
        {
            UIManager.Instance.HidePanel(UIPanel.SitPanel);
        }
    }
}
