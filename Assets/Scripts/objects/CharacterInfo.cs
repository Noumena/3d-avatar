﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SingleTon.scripts;

public class CharacterInfo : MonoBehaviour {

   [SerializeField] private Text characterNameText;
   [SerializeField] private CharacterType characterType;

    public CharacterType CharacterType
    {
        set { characterType = value; }
        get { return characterType; }
    }

    private string name;

    public string Name
    {
        set { name = value; }
        get { return name; }
    }

    private void Start()
    {
        if (characterNameText)
        {
            name = CharacterManager.Instance.GetSelectedCharacter().Name;
            characterNameText.text = name;
        }
    }

}
