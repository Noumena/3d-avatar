﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClampName : MonoBehaviour
{
    [SerializeField] private Text nameText;

    void FixedUpdate()
    {
        Vector3 namePos = Camera.main.WorldToScreenPoint(transform.position);
        nameText.transform.position = namePos;
    }
}
