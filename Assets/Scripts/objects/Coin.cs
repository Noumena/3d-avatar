﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class Coin : MonoBehaviour
{
    [SerializeField] private int score = 1;

    void Update()
    {
        transform.Rotate(1, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();

        if (player)
        {
            ScoreManager.Instance.AddScore(score);
            Destroy(gameObject);
        }
    }
}
