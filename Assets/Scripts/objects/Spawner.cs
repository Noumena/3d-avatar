﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingleTon.scripts;

public class Spawner : MonoBehaviour
{
    [SerializeField] CharacterInfo ajPrefab, kayaPrefab;
    private Dictionary<CharacterType, CharacterInfo> characters = new Dictionary<CharacterType, CharacterInfo>();
    private CharacterManager characterManager;

    private void Awake()
    {
        SpawnCharacter();
    }

    private void SpawnCharacter()
    {
        characterManager = CharacterManager.Instance;

        if (characterManager)
        {
            characters.Add(CharacterType.AJ, ajPrefab);
            characters.Add(CharacterType.Kaya, kayaPrefab);

            CharacterInfo selectedCharacter = characterManager.GetSelectedCharacter();
            Instantiate(characters[selectedCharacter.CharacterType], transform.position, Quaternion.identity);
        }
    }
}
